package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {


    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random rand;


    public Pokemon(String name){
        this.name = name;
        this.rand = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * rand.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * rand.nextGaussian()));

    }




    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if(healthPoints != 0)
            return true;
        else
            return false;
    }

    @Override
    public void attack(IPokemon target) {
        System.out.println(this.getName() + " attacks " + target.getName() + ".");

        int damageInflicted = (int) (this.strength + this.strength / 2 * rand.nextGaussian());
        target.damage(damageInflicted);

        if(!target.isAlive())
            System.out.println(target.getName() + " is defeated by " + this.getName() + ".");
    }

    @Override
    public void damage(int damageTaken) {
        if(damageTaken < 0)
            damageTaken = 0;
        healthPoints -= damageTaken;
        if(healthPoints < 0)
            healthPoints = 0;
        System.out.println(getName() + " takes " + damageTaken + " damage and is left with " + getCurrentHP() + "/" + getMaxHP() + " HP");
    }

    @Override
    public String toString() {
        return getName() + " HP: (" + getCurrentHP() + "/" + getMaxHP() + ") STR: " + getStrength();
    }

}
